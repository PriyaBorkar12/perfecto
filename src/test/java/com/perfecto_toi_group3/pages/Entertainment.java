package com.perfecto_toi_group3.pages;

import java.util.List;

// import com.flipkartapptest.components.ProductlistcomponentTestPage;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class Entertainment extends WebDriverBaseTestPage<WebDriverTestPage> {
	@FindBy(locator = "list.bollywood")
	private List<QAFWebElement> productlist;

	public List<QAFWebElement> getProductlist() {
		return productlist;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
	}

	@QAFTestStep(description = "user print the list of movie reviews")
	public void userPrintTheListOfMovieReviews() {
		waitForPageToLoad();
		for (int i = 0; i < getProductlist().size(); i++) {
			Reporter.log("Movie name :   " + getProductlist().get(i).getText());
		}

	}

}
