/**
 * 
 */
package com.perfecto_toi_group3.pages;

import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;

/**
 * @author Megha.Garg
 */
public class IPL {

	@QAFTestStep(description = "user clicks on options button of top stories")
	public void userClicksOnOptionsButtonOfTopStories() {
	}

	@QAFTestStep(description = "user verify saved stories")
	public void userVerifySavedStories() {
	}

}
