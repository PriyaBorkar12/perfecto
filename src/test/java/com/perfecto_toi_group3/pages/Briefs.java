package com.perfecto_toi_group3.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class Briefs extends WebDriverBaseTestPage<WebDriverTestPage> {
	@FindBy(locator = "btn.headlines.toppage")
	private QAFWebElement headLine;

	public QAFWebElement getHeadLine() {
		return headLine;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
	}

	@QAFTestStep(description = "user clicks on news headline")
	public void userClicksOnNewsHeadline() {
		waitForPageToLoad();
		getHeadLine().click();
		String headLines = getHeadLine().getText();
	}

	@QAFTestStep(description = "user clicks on back button of news")
	public void userClicksOnBackButtonOfNews() {
		
	}

	@QAFTestStep(description = "user clicks on save item button")
	public void userClicksOnSaveItemButton() {
		
		
		String a=String.format(ConfigurationManager.getBundle().getString("contentdesc.all",
				"Save"));
		System.out.println(new QAFExtendedWebElement(a).isDisplayed());
	
		/*if(new QAFExtendedWebElement(a).isPresent())
		{
			click(a);
			Reporter.logWithScreenShot(a);
		}*/
		
		//click(String.format(ConfigurationManager.getBundle().getString("contentdesc.all","Save")));
		
	
	}

}
