/**
 * 
 */
package com.perfecto_toi_group3.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.SendKeysAction;

import com.perfecto.sample.utils.PerfectoUtilities;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

/**
 * @author Megha.Garg
 */
public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}
	public QAFExtendedWebElement creatElement(String loc, String key) {

		return new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), key));
	}

	@QAFTestStep(description = "user opens the application")
	public void userOpensTheApplication() {
		PerfectoUtilities.openApplication("TOI");
	}

	@QAFTestStep(description = "user clicks on {0}")
	public void userClicksOn(String btn) {
		//CommonStep.waitForPresent("contentdesc.all");
		click(String.format(ConfigurationManager.getBundle().getString("contentdesc.all"),
				btn));

	}
	@QAFTestStep(description = "user clicks on {0} button")
	public void Small(String btn) {
		click(String.format(ConfigurationManager.getBundle().getString("text.all"), btn));
	}

	@QAFTestStep(description = "user search for {0} and text is {1}")
	public void userSearchFor(String loc) {

		new QAFExtendedWebElement(loc).sendKeys("");

	}
	
	@QAFTestStep(description = "user clicks on {0} button and send character {1}")
	public void userClicksOnButtonAndSendCharacter(String btn, String text) {
		String loc=String.format(ConfigurationManager.getBundle().getString("text.all",
				btn));
			CommonStep.sendKeys(text,loc);
			
			
			
			//AFExtendedWebElement ele1 =new QAFExtendedWebElement();
			//ele1.sendKeys(Keys.ENTER);
			
			
			
			
		
	}

}
